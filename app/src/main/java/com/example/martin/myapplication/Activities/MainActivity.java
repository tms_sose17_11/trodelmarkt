package com.example.martin.myapplication.Activities;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;

import com.example.martin.myapplication.Adapters.AppointmentAdapter;
import com.example.martin.myapplication.BuildConfig;
import com.example.martin.myapplication.FutureDate;
import com.example.martin.myapplication.Adapters.GroupListAdapter;
import com.example.martin.myapplication.JasonParser;
import com.example.martin.myapplication.PickDate;
import com.example.martin.myapplication.PrefUtils;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;
import com.example.martin.myapplication.ServerCommunication.ServerResponse;
import com.google.android.gms.common.ConnectionResult;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;

import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import DataObjects.Appointmant;
import DataObjects.Group;
import DataObjects.JunkMarket;


public class MainActivity extends AppCompatActivity implements  GoogleApiClient.ConnectionCallbacks, ServerResponse, FutureDate<Integer>,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {

    private final String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    protected static final int CENTER_OWN_POSITION_COUNT_DOWN = 4;
    protected static final int REQUEST_CODE_RESOLUTION = 1;
    private static final int PERMISSIONS_REQUEST = 1;
    public static final String INTANT_TAG_SELECTED_MARKET_ID = "selectedMarketID";
    public static final String INTANT_TAG_APPOINTMENT = "Appointment";
    private static final String PARSEXCEPTION = "PARSEXCEPTION";
    private static final String KEY_IN_RESOLUTION = "is_in_resolution";
    //Berlin Fernsehrturm default Location
    private static final double DEFAULT_LATITUDE = 52.5208182;
    private static final double DEFAULT_LONGITUDE = 13.4072303;


    private boolean isInResolution;
    private ProgressDialog pd;
    private MenuItem menuItem_appointment_invitations;
    private MenuItem menuItem_group_invitations;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private boolean centerMap = false;
    private ArrayList<JunkMarket> junkMarkets;
    private ItemizedOverlayWithFocus<OverlayItem> overlay;
    private MapView mapView;
    private IMapController mapController;
    private List<OverlayItem> items;
    private Location currrentLocation = null;
    private ArrayList<Group> unconfirmedGroups;
    private ArrayList<Appointmant> unconfirmedAppointments;
    private Requests requests;
    private int multibleMarketsOnSamePosition = 0;
    private int selectedMarketID;
    private int centerOwnPositionCountdown = CENTER_OWN_POSITION_COUNT_DOWN;
    private static Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestPermissions();
        if (savedInstanceState != null) {
            isInResolution = savedInstanceState.getBoolean(KEY_IN_RESOLUTION, false);
        }
        setContentView(R.layout.activity_main);
        pd = new ProgressDialog(this, R.style.MyTheme);
        pd.setCancelable(false);
        unconfirmedGroups = new ArrayList<Group>();
        unconfirmedAppointments = new ArrayList<Appointmant>();
        requests = new Requests(this);
        initMap();
        initLocationRequest();
        initJunkmarketItems();
        overlay = new ItemizedOverlayWithFocus<OverlayItem>(items, this, this);
        overlay.setFocusItemsOnTap(true);
        mapView.getOverlays().add(overlay);
        selectedMarketID = getIntent().getIntExtra(INTANT_TAG_SELECTED_MARKET_ID, -1);
        CheckInvitationTask myTask = new CheckInvitationTask();
        timer = new Timer();
        timer.scheduleAtFixedRate(myTask, 0, 10000);

    }
    private void initLocationRequest(){
        locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(10000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    private void initMap(){
        mapView = (MapView) findViewById(R.id.map);
        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);
        mapController = mapView.getController();
        mapController.setZoom(new Integer(15));
    }
    private void initJunkmarketItems(){
        junkMarkets = new JasonParser().getMarkets(this);
        items = new ArrayList<OverlayItem>();
        NumberFormat format = NumberFormat.getInstance(Locale.GERMAN);
        for (JunkMarket jm : junkMarkets) {
            try {
                Number numberLat = format.parse(jm.getLatitude());
                Number numberLng = format.parse(jm.getLongitude());
                OverlayItem market = new OverlayItem(jm.getBetreiber(), String.valueOf(jm.getId()), new GeoPoint(numberLat.doubleValue(), numberLng.doubleValue()));
                market.setMarker(getDrawable(R.drawable.market_icon));
                items.add(market);
            } catch (ParseException e) {
                Log.e(PARSEXCEPTION, e.getMessage());
            }
        }
    }
    public int getUngrantedPermissionIndex(){
        for (int i = 0; i < PERMISSIONS.length; i++) {
            if (checkSelfPermission(PERMISSIONS[i]) != PackageManager.PERMISSION_GRANTED)
                return i;
        }
        return -1;
    }
    public void requestPermissions() {
        int permisson = getUngrantedPermissionIndex();
        if(permisson != -1) {
            requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }
                return;
            }
        }
    }

    private void setVisibilitieAppointmentInvitationIcon(){
        if(menuItem_appointment_invitations != null) {
            if (unconfirmedAppointments.isEmpty() == false) {
                menuItem_appointment_invitations.setVisible(true);
            } else {
                menuItem_appointment_invitations.setVisible(false);
            }
            this.invalidateOptionsMenu();
        }
    }
    private void setVisibilitieGroupMenuIcon(){
        if(menuItem_group_invitations != null) {
            if (unconfirmedGroups.isEmpty() == false) {
                menuItem_group_invitations.setVisible(true);
            } else {
                menuItem_group_invitations.setVisible(false);
            }
            this.invalidateOptionsMenu();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        menuItem_group_invitations = menu.findItem(R.id.new_group_invitations_menu_icon);
        menuItem_appointment_invitations = menu.findItem(R.id.new_appointment_invitation_menu_icon);
        setVisibilitieGroupMenuIcon();
        setVisibilitieAppointmentInvitationIcon();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i = null;
        switch (item.getItemId()) {
            case R.id.action_register:
                i = new Intent(this, RegisterActivity.class);
                timer = null;
                startActivity(i);
                break;
            case R.id.action_login:
                i = new Intent(this, LoginActivity.class);
                timer = null;
                startActivity(i);
                break;
            case R.id.action_login_out:
                PrefUtils.deletefromPrefs(this,  PrefUtils.PREFS_LOGIN_TOKEN_KEY );
                Toast.makeText(getApplicationContext(),
                        R.string.logout_messsege, Toast.LENGTH_LONG)
                        .show();
                break;
            case R.id.action_groups:
                i = new Intent(MainActivity.this, GroupsActivity.class);
                timer  = null;
                startActivity(i);
                break;
            case R.id.new_group_invitations_menu_icon:
                showGroupInvitationDialog();
                break;
            case R.id.new_appointment_invitation_menu_icon:
                showdAppointmentInvitationDialog();
                break;
            default:
                break;

        }

        return true;
    }
    private void showGroupInvitationDialog(){
        final Dialog GroupInvitationDialog = new Dialog(this);
        GroupInvitationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        GroupInvitationDialog.setContentView(R.layout.invitation_confirm_dialog);
        final ListView groupListview = (ListView) GroupInvitationDialog.findViewById(R.id.listview_invitations);
        final GroupListAdapter groupListAdapter = new GroupListAdapter(this, unconfirmedGroups, GroupListAdapter.SELECT_GROUPS_FOR_CONFRIM_INVITATION_VIEW);
        groupListview.setAdapter(groupListAdapter);
        Button btnSaveGroupInvitations = (Button) GroupInvitationDialog.findViewById(R.id.btn_save_invitation);
        btnSaveGroupInvitations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                requests.confirmGroupInvitations(groupListAdapter.getCheckeGroupsIDs());
                GroupInvitationDialog.dismiss();
            }
        });
        GroupInvitationDialog.show();
    }
    private void showdAppointmentInvitationDialog(){
        final Dialog dialogAppointmentInvitation = new Dialog(this);
        dialogAppointmentInvitation.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAppointmentInvitation.setContentView(R.layout.invitation_confirm_dialog);
        ListView appointmentsListview = (ListView) dialogAppointmentInvitation.findViewById(R.id.listview_invitations);
        final AppointmentAdapter appointmentAdapter= new AppointmentAdapter(this, unconfirmedAppointments, AppointmentAdapter.SELECT_APPOINTMENTS_VIEW);
        appointmentsListview.setAdapter(appointmentAdapter);
        Button btnSaveAppointmentInvitations = (Button) dialogAppointmentInvitation.findViewById(R.id.btn_save_invitation);
        btnSaveAppointmentInvitations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                requests.confirmAppointmentInvitations(appointmentAdapter.getCheckeAppointmentIDs());
                dialogAppointmentInvitation.dismiss();
            }
        });
        dialogAppointmentInvitation.show();
    }
    private JunkMarket getJunkMarket(int id){
        for(JunkMarket jm : junkMarkets){
            if(id == jm.getId()){
                return jm;
            }
        }
        return null;
    }

    private void showToastForMultibleMarketsOnSamePosition(JunkMarket junkMarket){
        if (junkMarket.getMultibleMarketsOnThisPlace().isEmpty() == false ) {
            if(multibleMarketsOnSamePosition == 0) {
                multibleMarketsOnSamePosition = junkMarket.getMultibleMarketsOnThisPlace().size();
                Toast.makeText(getApplicationContext(), String.format(getResources().getString(R.string.multible_markets_on_same_position_message), Integer.toString((multibleMarketsOnSamePosition + 1))) , Toast.LENGTH_LONG)
                        .show();
            }else{
                multibleMarketsOnSamePosition--;
            }
        }
    }
    private void showJunkmarketInfoDialog(final JunkMarket junkMarket){
        showToastForMultibleMarketsOnSamePosition(junkMarket);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.junk_market_dialog);

        TextView txtContenDistric = (TextView) dialog.findViewById(R.id.txt_content_district);
        txtContenDistric.setText(junkMarket.getBezirk());

        TextView txtContentLocation = (TextView) dialog.findViewById(R.id.txt_content_location);
        txtContentLocation.setText(junkMarket.getLocation());

        TextView txtContentDays = (TextView) dialog.findViewById(R.id.txt_content_days);
        txtContentDays.setText(junkMarket.getTage());

        TextView txtContentTime = (TextView) dialog.findViewById(R.id.txt_content_time);
        txtContentTime.setText(junkMarket.getZeiten());

        TextView txtContentCarrier = (TextView) dialog.findViewById(R.id.txt_content_carrier);
        txtContentCarrier.setText(junkMarket.getBetreiber());

        TextView txtContentEmail = (TextView) dialog.findViewById(R.id.txt_content_email);
        txtContentEmail.setText(junkMarket.getEmail());

        TextView txtContentWWW = (TextView) dialog.findViewById(R.id.txt_content_www);
        txtContentWWW.setText(junkMarket.getWww());

        TextView txtContentComment = (TextView) dialog.findViewById(R.id.txt_content_comment);
        txtContentComment.setText(junkMarket.getBemerkungen());

        Button btnRating = (Button) dialog.findViewById(R.id.btn_see_ratings);
        btnRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent i = new Intent(MainActivity.this, RatingActivity.class);
                i.putExtra(INTANT_TAG_SELECTED_MARKET_ID, junkMarket.getId());
                startActivity(i);
            }
        });
        final PickDate<Integer> pickDate = new PickDate<Integer>(MainActivity.this, this, junkMarket.getId());

        Button btnVisit = (Button) dialog.findViewById(R.id.btn_visit);
        btnVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pickDate.pickDateDialogForFuturDate(getString(R.string.date_must_be_in_future_error_message));
            }
        });
        dialog.show();
    }
    @Override
    public boolean onItemSingleTapUp(int index, final OverlayItem item) {
        if(item.getTitle().equals(getString(R.string.own_position_item_titel)) == false) {
            JunkMarket junkMarket = getJunkMarket(Integer.parseInt(item.getSnippet()));
            showJunkmarketInfoDialog(junkMarket);
        }
        return false;
    }


    @Override
    public boolean onItemLongPress(int index, OverlayItem item) {

        return false;
    }

    @Override
    public void publishResult(JSONObject data) {
        try {
            if (data.getString(Requests.TYPE).equals(Requests.UNCONFIRMED_INVITATION)) {
                ArrayList<Group> groups = new Gson().fromJson(data.getString(Requests.GROUPS), new TypeToken<List<Group>>() {
                }.getType());
                if(groups != null)
                    unconfirmedGroups = groups;
                ArrayList<Appointmant> appointments = new Gson().fromJson(data.getString(Requests.APPOINTMENTTS), new TypeToken<List<Appointmant>>() {
                }.getType());
                if(appointments != null)
                    unconfirmedAppointments = appointments;
            }
            if (data.getString(Requests.TYPE).equals(Requests.SUCCESS)) {
                unconfirmedGroups.clear();
                unconfirmedAppointments.clear();
                pd.dismiss();
                Toast.makeText(getApplicationContext(),
                        data.getString(Requests.MESSAGE), Toast.LENGTH_LONG)
                        .show();
            }
            setVisibilitieGroupMenuIcon();
            setVisibilitieAppointmentInvitationIcon();
        }catch (JSONException e){
            Log.e(PARSEXCEPTION, e.getMessage());
        }

    }

    private void updateIcons() {
        if(googleApiClient.isConnected()) {
            OverlayItem overlayItem = new OverlayItem(getString(R.string.own_position_item_titel), getString(R.string.own_position_item_snippet), getLastLocationPoint());
            overlayItem.setMarker(getDrawable(R.drawable.you_icon));
            if(items.get(items.size()-1).getTitle().equals(getString(R.string.own_position_item_titel))) {
                items.remove(items.size()-1);
                items.add(overlayItem);
            }else{
                items.add(overlayItem);
            }
            overlay = new ItemizedOverlayWithFocus<OverlayItem>(items, this, this);
            mapView.getOverlays().clear();
            mapView.getOverlays().add(overlay);
            mapView.invalidate();
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        centerMap();
    }
    private GeoPoint getLastLocationPoint() {
        Location location = null;

        if  (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)  == PackageManager.PERMISSION_GRANTED){
            location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        }else{
           requestPermissions();
        }
        double lat;
        double lng;
        if(location != null) {
            currrentLocation = location;
            lat = currrentLocation.getLatitude();
            lng = currrentLocation.getLongitude();

        }else{
            lat = DEFAULT_LATITUDE;
            lng = DEFAULT_LONGITUDE;

        }
        return new GeoPoint(lat, lng);
    }

    private void centerMap() {
        if(centerMap && googleApiClient.isConnected()) {
            mapController.setCenter(getLastLocationPoint());
            mapView.invalidate();
            centerMap = false;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        centerMap = true;
        CheckInvitationTask myTask = new CheckInvitationTask();
        timer = new Timer();
        timer.scheduleAtFixedRate(myTask, 0, 10000);
        Log.d("test", "onsResume");

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(googleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        timer = null;
        Log.d("test", "onPaus");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        googleApiClient.connect();
        centerMap = true;
        showSelectetMarket();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocationServices.FusedLocationApi.removeLocationUpdates(
                googleApiClient, this);
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        timer = null;
    }

    @Override
    public void onConnectionSuspended(int cause) {
        retryConnecting();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(
                    result.getErrorCode(), this, 0, new OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            retryConnecting();
                        }
                    }).show();
            return;
        }
        if (isInResolution) {
            return;
        }
        isInResolution = true;
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            retryConnecting();
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_IN_RESOLUTION, isInResolution);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_RESOLUTION:
                retryConnecting();
                break;
        }
    }

    private void retryConnecting() {
        isInResolution = false;
        if (!googleApiClient.isConnecting()) {
            googleApiClient.connect();
        }

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if  (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)  == PackageManager.PERMISSION_GRANTED){
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }else {
            requestPermissions();
        }
        centerMap();
        updateIcons();
    }
    private void cancelOtherSelectedMarket(){
        for (OverlayItem item :  items){
            if(item.getDrawable().getConstantState().equals(getDrawable(R.drawable.selected_market_icon).getConstantState()) == true){
                item.setMarker(getDrawable(R.drawable.market_icon));
            }
        }

    }
    public void setSelectedMarketID(int newMartketID){
        selectedMarketID = newMartketID;
        centerOwnPositionCountdown =  CENTER_OWN_POSITION_COUNT_DOWN;

    }
    private OverlayItem getOverlayItemMarket(int id){
        int i =0;
        for(OverlayItem jm : items){
            i++;
            if(jm.getTitle().equals(getString(R.string.own_position_item_titel)) == false) {
                if (Integer.parseInt(jm.getSnippet()) == id) {
                    return jm;
                }
            }
        }
        return null;
    }
    private void centerOwnPosition(){
        showSelectetMarket();
        if(selectedMarketID != -1 ){
            centerOwnPositionCountdown--;
            if(centerOwnPositionCountdown == 0) {
                OverlayItem market = getOverlayItemMarket(selectedMarketID);
                if(market != null) {
                    market.setMarker(getDrawable(R.drawable.market_icon));
                    items.set(items.indexOf(market), market);
                }
                selectedMarketID = -1;
                centerOwnPositionCountdown = CENTER_OWN_POSITION_COUNT_DOWN;
                mapController.animateTo(getLastLocationPoint());
            }
        }
    }
    public void showSelectetMarket(){
        if(selectedMarketID != -1 && centerOwnPositionCountdown == CENTER_OWN_POSITION_COUNT_DOWN) {
            cancelOtherSelectedMarket();
            OverlayItem market = getOverlayItemMarket(selectedMarketID);
            if(market != null) {
                mapController.animateTo(market.getPoint());
                market.setMarker(getDrawable(R.drawable.selected_market_icon));
                int position = items.indexOf(market);
                for (Integer i : getJunkMarket(selectedMarketID).getMultibleMarketsOnThisPlace()){
                    OverlayItem marketOther = getOverlayItemMarket(i);
                    int positionTemp = items.indexOf(marketOther);
                    if(position > positionTemp)
                        Collections.swap(items, positionTemp, position);
                }
                items.set(items.indexOf(market), market);
            }
        }

    }

    @Override
    public void isFuturDate(Calendar calendar, Integer pram) {
        Intent i = new Intent(MainActivity.this, GroupsActivity.class);
        i.putExtra(INTANT_TAG_APPOINTMENT, calendar);
        i.putExtra(INTANT_TAG_SELECTED_MARKET_ID, pram);
        startActivity(i);
        Toast.makeText(getApplicationContext(),
                R.string.select_group_for_appointment_registration_hint, Toast.LENGTH_LONG)
                .show();
    }


    private class CheckInvitationTask extends TimerTask {
        public void run() {
            requests.checkInvatations();
            centerOwnPosition();
            Log.d("test","test");
            if(timer == null)
                cancel();
        }
    }
}


