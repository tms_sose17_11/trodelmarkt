package com.example.martin.myapplication.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;
import com.example.martin.myapplication.ServerCommunication.ServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import DataObjects.User;

public class RegisterActivity extends AppCompatActivity implements ServerResponse {

    private EditText etxtInputFirstName;
    private EditText etxtInputSurname;
    private EditText etxtInputUserName;
    private EditText etxtInputPassword;
    private ProgressDialog pd;
    private Requests requests;


    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        requests = new Requests(this);
        etxtInputFirstName = (EditText) findViewById(R.id.etxt_register_first_name);
        etxtInputSurname = (EditText) findViewById(R.id.etxt_register_surname);
        etxtInputUserName = (EditText) findViewById(R.id.etxt_register_username);
        etxtInputPassword = (EditText) findViewById(R.id.etxt_register_password);
        pd = new ProgressDialog(RegisterActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_register_link_to_login_screen:
                Intent i = new Intent(RegisterActivity.this,
                        LoginActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.btn_register:
                String firstName = etxtInputFirstName.getText().toString().trim();
                String surname = etxtInputSurname.getText().toString().trim();
                String userName = etxtInputUserName.getText().toString().trim();
                String password = etxtInputPassword.getText().toString().trim();
                if (!firstName.isEmpty() && !surname.isEmpty() && !userName.isEmpty() && !password.isEmpty()) {
                    pd.show();
                    User newUser = new User(firstName, surname, userName, password);
                    requests.registerUser(newUser);
                } else {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.missing_information_error_message), Toast.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void publishResult(JSONObject data) {
        pd.dismiss();
        try {
            if (data.getString(Requests.TYPE).equals(Requests.SUCCESS)) {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                Toast.makeText(getApplicationContext(), data.getString(Requests.MESSAGE), Toast.LENGTH_LONG)
                        .show();
                finish();
            } else if (data.getString(Requests.TYPE).equals(Requests.FAIL)) {
                Toast.makeText(getApplicationContext(), data.getString(Requests.MESSAGE), Toast.LENGTH_LONG)
                        .show();

            }
        } catch (JSONException e) {
            Log.e(Requests.JSONEXCEPTION, e.getMessage());

        }
    }
}
