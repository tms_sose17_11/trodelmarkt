package com.example.martin.myapplication;

import android.content.Context;
import android.util.Log;

import com.example.martin.myapplication.ServerCommunication.Requests;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import DataObjects.JunkMarket;

/**
 * Created by martin on 11.05.17.
 */

public class JasonParser {

    public ArrayList<JunkMarket> getMarkets(Context context) {
        ArrayList<JunkMarket> junkMarkets = null;
        try {
            JSONObject file = new JSONObject(loadJSONFromAsset(context));
            junkMarkets = new Gson().fromJson(file.getString("index"), new TypeToken<List<JunkMarket>>() {
            }.getType());
        } catch (JSONException e) {
            Log.d(Requests.JSONEXCEPTION, e.getMessage());

        }
        return junkMarkets;
    }


    private String loadJSONFromAsset(Context context) {
        String json = null;
        try {

            InputStream is = context.getAssets().open("troedlemaerkteWithDublicate_formated.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            return json;
        }
        return json;

    }
}
