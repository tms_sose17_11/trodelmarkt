package com.example.martin.myapplication;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by martin on 01.06.17.
 */

public class PickDate<T> {

    private Calendar calendar;
    private Context context;
    private FutureDate futureDate;
    private T param;

    public PickDate(Context context, FutureDate<T> futureDate, T param) {
        this.context = context;
        this.futureDate = futureDate;
        this.param = param;
    }

    public void pickDateDialogForFuturDate(final String errorMessage) {
        calendar = Calendar.getInstance();
        Locale.setDefault(Locale.GERMANY);
        int mYear = calendar.get(Calendar.YEAR);
        final int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth, 23, 59);
                if (calendar.getTime().after(new Date())) {
                    pickTimeDialogForFuturDate(errorMessage);
                } else {
                    Toast.makeText(context,
                            errorMessage, Toast.LENGTH_LONG)
                            .show();
                    pickDateDialogForFuturDate(errorMessage);
                }
            }

        }, mYear, mMonth, mDay);
        dialog.show();
    }

    private void pickTimeDialogForFuturDate(final String errorMessage) {
        Locale.setDefault(Locale.GERMANY);

        TimePickerDialog dialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {

            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                if (calendar.getTime().after(new Date())) {
                    futureDate.isFuturDate(calendar, param);
                } else {
                    Toast.makeText(context,
                            errorMessage, Toast.LENGTH_LONG)
                            .show();
                    pickTimeDialogForFuturDate(errorMessage);
                }
            }

        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);


        dialog.show();
    }

}
