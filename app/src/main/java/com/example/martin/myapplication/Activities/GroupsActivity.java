package com.example.martin.myapplication.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.martin.myapplication.Adapters.GroupListAdapter;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;
import com.example.martin.myapplication.ServerCommunication.ServerResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import DataObjects.Group;

public class GroupsActivity extends AppCompatActivity implements ServerResponse {
    public static final String INTANT_TAG_GROUP = "group";
    public static final String INTANT_TAG_USERNAME = "username";

    private ListView listView;
    private GroupListAdapter groupListAdapter;
    private ProgressDialog pd;
    private EditText etxtGroupName;
    private ArrayList<Group> groups;
    private Button btnCreateGroup;
    private Requests requests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        pd = new ProgressDialog(GroupsActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
        requests = new Requests(this);
        groups = new ArrayList<Group>();
        getGroups();
        findViews();
        Calendar appointmentDate = (Calendar) getIntent().getSerializableExtra(MainActivity.INTANT_TAG_APPOINTMENT);
        int marketID = getIntent().getIntExtra(MainActivity.INTANT_TAG_SELECTED_MARKET_ID, -1);
        if (isViewForRegisterAppoinment(appointmentDate, marketID) == false) {
            groupListAdapter = new GroupListAdapter(this, groups, GroupListAdapter.ADMINISTRATE_GROUPS_VIEW);
            listView.setAdapter(groupListAdapter);
        }
    }

    private boolean isViewForRegisterAppoinment(final Calendar appointmentDate, final int marketID) {
        if (appointmentDate != null && marketID != -1) {
            removeGroupNameEtxtCreateGroupBtn();
            groupListAdapter = new GroupListAdapter(this, groups, GroupListAdapter.SELECT_GROUP_FOR_REGISTER_APPOINTMENT_VIEW);
            listView.setAdapter(groupListAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> av, View v, int position, long id) {
                    Group g = groups.get(position);
                    pd.show();
                    requests.registerAppointment(appointmentDate, marketID, g.getId());
                }
            });
            return true;
        } else
            return false;
    }

    private void findViews() {
        etxtGroupName = (EditText) findViewById(R.id.etxt_group_name);
        btnCreateGroup = (Button) findViewById(R.id.btn_create_group);
        listView = (ListView) findViewById(R.id.groups_listview);
    }

    private void removeGroupNameEtxtCreateGroupBtn() {
        LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.groups_LinearLayout_1);
        if (btnCreateGroup != null && etxtGroupName != null) {
            linearLayout1.removeView(btnCreateGroup);
            linearLayout1.removeView(etxtGroupName);
        }
    }

    public void progressDialogShow() {
        pd.show();
    }

    private void getGroups() {
        pd.show();
        requests.getGroupsContainingUser();
    }

    public void onClick(View view) {
        pd.show();
        if (etxtGroupName.getText().equals("") == false) {
            requests.createGroup(etxtGroupName.getText().toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getGroups();
    }

    @Override
    public void publishResult(JSONObject data) {
        pd.dismiss();
        try {
            switch (data.getString(Requests.TYPE)) {
                case Requests.GROUPS:
                    groups = new Gson().fromJson(data.getString(Requests.GROUPS), new TypeToken<List<Group>>() {
                    }.getType());
                    groupListAdapter.setCurrentLogedInUsername(data.getString(Requests.USERNAME));
                    groupListAdapter.setGroups(groups);
                    groupListAdapter.notifyDataSetInvalidated();
                    break;
                case Requests.SUCCESS:
                    Toast.makeText(getApplicationContext(), data.getString(Requests.MESSAGE), Toast.LENGTH_LONG).show();
                    getGroups();
                    break;
                case Requests.SUCCEESS_REGISTERT_APPOINTMENT:
                    this.finish();
                    break;
                case Requests.FAIL:
                    Toast.makeText(getApplicationContext(), data.getString(Requests.MESSAGE), Toast.LENGTH_LONG).show();
                default:
                    break;
            }
        } catch (JSONException e) {
            Log.e(Requests.JSONEXCEPTION, e.getMessage());
        }


    }

}