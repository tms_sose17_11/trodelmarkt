package com.example.martin.myapplication.ServerCommunication;

import org.json.JSONObject;

/**
 * Created by martin on 22.04.17.
 */

public interface ServerResponse {
    public void publishResult(JSONObject data);
}
