package com.example.DataObjects;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by martin on 04.05.17.
 */

public class LoginSys implements Serializable {
    private String userName;
    private UUID token;
    private Date expiryDate;

    public LoginSys(String userName) {
        this.userName = userName;
        this.token = UUID.randomUUID();
        this.expiryDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(this.expiryDate);
        c.add(Calendar.DATE, 1);
        this.expiryDate = c.getTime();
    }

    public boolean checklogin(UUID token) {
        Date now = new Date();
        if (this.token.equals(token) && now.before(this.expiryDate) == true) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, 1);
            this.expiryDate = c.getTime();
            System.out.println(print());
            return true;
        } else
            return false;
    }

    public String print() {
        return "Date: " + expiryDate.toString() + " token: " + token.toString() + " username: " + userName;

    }

    public String getUserName() {
        return userName;
    }

    public UUID getToken() {
        return token;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }
}
