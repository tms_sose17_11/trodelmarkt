package com.example.Server;


import com.example.DataObjects.Appointmant;
import com.example.DataObjects.Group;
import com.example.DataObjects.Rating;
import com.example.DataObjects.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

class ServerThread extends Thread implements Protokoll {

    private static final String TAG = ServerThread.class.getName();

    private Server server = null;
    private Socket socket = null;
    private int ID = -1;
    private DataInputStream streamIn = null;
    private DataOutputStream streamOut = null;
    private DbHelper dbHelper;

    public ServerThread(Server _server, Socket _socket, DbHelper dbHelper) {
        super();
        server = _server;
        socket = _socket;
        this.dbHelper = dbHelper;
        ID = socket.getPort();


    }

    private synchronized JSONObject registerUserResponse(boolean success) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        if (success) {
            response.put(TYPE, SUCCESS);
            response.put(MESSAGE, ServerMessages.SUCCESSFULL_REGESTRATION.getMessage());
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.USER_ALREADY_EXCISTING.getMessage());
        }
        return response;
    }

    private boolean registerUserRequest(JSONObject data) throws JSONException, IOException {
        data.remove(TYPE);
        User newUser = new Gson().fromJson(data.getString(NEWUSER), User.class);
        return dbHelper.addUser(newUser);
    }


    private synchronized JSONObject loginUserRequest(JSONObject data) throws JSONException, IOException {
        data.remove(TYPE);
        UUID token = dbHelper.checkUserlogin(data.getString(USERNAME), data.getString(PASSWORD));
        JSONObject response = new JSONObject();
        if (token != null) {
            response.put(TYPE, SUCCESS);
            response.put(MESSAGE, ServerMessages.SUCCESSFULL_LOGIN.getMessage());
            response.put(TOKEN, token);
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.FAIL_LOGIN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject newRatingRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            Rating newRating = new Rating(data.getInt(MARKET_ID), u.getUsername(), data.getInt(RATING_FOR_MARKET), data.getString(COMMENT_FOR_RATING));
            if (dbHelper.addRating(newRating) == true) {
                response.put(TYPE, SUCCESS);
                response.put(MESSAGE, ServerMessages.SUCCESSFULLY_SAVE_RATING.getMessage());
            } else {
                response.put(TYPE, FAIL);
                response.put(MESSAGE, ServerMessages.ALLREADY_RATED.getMessage());
            }
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject getRatingsRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        response.put(TYPE, RATINGS);
        response.put(RATINGS, new Gson().toJson(dbHelper.getRatings(data.getInt(MARKET_ID))));
        return response;

    }

    private synchronized JSONObject getGroupsRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        response.put(TYPE, GROUPS);
        User user = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (user != null) {
            response.put(GROUPS, new Gson().toJson(dbHelper.getGroups(user)));
            response.put(USERNAME, user.getUsername());
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject createNewGroupsRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            Group group = new Group(data.getString(GROUP_NAME), u.getUsername());
            group.addUserToGroup(u);
            dbHelper.addGroup(group);
            response.put(TYPE, SUCCESS);
            response.put(MESSAGE, ServerMessages.SUCCESSFULL_CREATED_GROUP.getMessage());
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject deleteGroupRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            Group g = dbHelper.getGroup(data.getInt(GROUP_ID));
            if (g != null) {
                if (dbHelper.deleteGroup(g, u.getUsername()) == true) {
                    response.put(TYPE, SUCCESS);
                    response.put(MESSAGE, ServerMessages.SUCCESSFULL_DELETE_GROUP.getMessage());
                } else {
                    response.put(TYPE, FAIL);
                    response.put(MESSAGE, ServerMessages.NOT_ADMIN.getMessage());
                }
            } else {
                response.put(TYPE, FAIL);
                response.put(MESSAGE, ServerMessages.GROUP_DOESNT_EXCIST.getMessage());
            }
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject changeMembersInGroupRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            User newFriend = dbHelper.getUser(new User(data.getString(USERNAME)));
            Group group = dbHelper.getGroup(data.getInt(GROUP_ID));
            System.out.println(group);
            if (group != null) {
                if (newFriend != null) {
                    if (data.getString(TYPE).equals(INVITE_FRIEND)) {
                        response = addFriend(group, newFriend, response);
                    } else if (data.getString(TYPE).equals(DELETE_FRIEND)) {
                        response = deleteFriend(group, newFriend, response);
                    }
                } else {
                    response.put(TYPE, FAIL);
                    response.put(MESSAGE, ServerMessages.USER_DOESNT_EXCIST.getMessage(data.getString(USERNAME)));
                }
            } else {
                response.put(TYPE, FAIL);
                response.put(MESSAGE, ServerMessages.GROUP_DOESNT_EXCIST.getMessage());
            }
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject deleteFriend(Group group, User newFriend, JSONObject response) throws JSONException {
        if (group.getUsers().contains(newFriend) == true) {
            group.getUsers().remove(newFriend);
            dbHelper.changeGroup(group);
            dbHelper.removeFriendFromAllAppointments(newFriend, group);
            response.put(TYPE, SUCCESS);
            response.put(GROUP, new Gson().toJson(dbHelper.getGroup(group.getId())));
            response.put(MESSAGE, ServerMessages.SUCCESSFULL_DELETE_uSER.getMessage(newFriend.getUsername()));
        }
        if (group.getInvitedUsers().contains(newFriend) == true) {
            group.getInvitedUsers().remove(newFriend);
            dbHelper.changeGroup(group);
            response.put(TYPE, SUCCESS);
            response.put(GROUP, new Gson().toJson(dbHelper.getGroup(group.getId())));
            response.put(MESSAGE, ServerMessages.SUCCESSFULL_DELETE_uSER.getMessage(newFriend.getUsername()));

        }

        return response;
    }

    private synchronized JSONObject addFriend(Group group, User newFriend, JSONObject response) throws JSONException {
        if (group.getUsers().contains(newFriend) == false) {
            if (group.getInvitedUsers().contains(newFriend) == false) {
                group.getInvitedUsers().add(newFriend);
                dbHelper.changeGroup(group);
                response.put(TYPE, SUCCESS);
                response.put(GROUP, new Gson().toJson(dbHelper.getGroup(group.getId())));
                response.put(MESSAGE, ServerMessages.USER_INVITED.getMessage(newFriend.getUsername()));

            } else {
                response.put(TYPE, FAIL);
                response.put(MESSAGE, ServerMessages.USER_ALREADY_INVITED_UNCONFIRMED.getMessage(newFriend.getUsername()));
            }
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.USER_ALREADY_INVITED_CONFIRMED.getMessage(newFriend.getUsername()));
        }
        return response;
    }

    private synchronized JSONObject checkInvitations(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User user = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (user != null) {
            ArrayList<Group> groups = dbHelper.getUnconfrimedGroups(user);
            ArrayList<Appointmant> unconfirmtAppointments = dbHelper.getUnconfirmtAppointments(user);
            response.put(TYPE, UNCONFIRMED_INVITATION);
            response.put(GROUPS, new Gson().toJson(groups));
            response.put(APPOINTMENTTS, new Gson().toJson(unconfirmtAppointments));
        } else {
            response.put(TYPE, FAIL);
        }
        return response;

    }

    private synchronized JSONObject confirmInvitations(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            ArrayList<Integer> groupIds = new Gson().fromJson(data.getString(CONFIRMED_GROUPIDS), new TypeToken<List<Integer>>() {
            }.getType());
            dbHelper.confirmGroupInvitation(groupIds, u);
            response.put(TYPE, SUCCESS);
            response.put(MESSAGE, ServerMessages.REFRESH_GROUP_INVITATION.getMessage());
        } else {
            response.put(TYPE, FAIL);
        }
        return response;

    }

    private synchronized JSONObject commandNotFound(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        response.put(TYPE, FAIL);
        response.put(MESSAGE, ServerMessages.NO_SERVICE_ERROR.getMessage());
        return response;

    }

    private synchronized JSONObject registerAppointmentRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            Group g = dbHelper.getGroup(data.getInt(GROUP_ID));
            if (g != null) {
                if (g.getUsers().contains(u) == true) {
                    dbHelper.addAppointment(g, new Gson().fromJson(data.getString(APPOINTMENT_DATE), Calendar.class), data.getInt(MARKET_ID), u);
                    response.put(TYPE, SUCCEESS_REGISTERT_APPOINTMENT);
                    response.put(MESSAGE, ServerMessages.SUCCESSFULL_APPOINTMENT_REGISTRATION.getMessage());
                } else {
                    response.put(TYPE, FAIL);
                    response.put(MESSAGE, ServerMessages.REGISTER_APOINTMENT_ERROR_NOT_A_MAMBER.getMessage());
                }
            } else {
                response.put(TYPE, FAIL);
                response.put(MESSAGE, ServerMessages.GROUP_DOESNT_EXCIST.getMessage());
            }
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject confirmAppointmentsRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {

            ArrayList<Integer> appointmentIDs = new Gson().fromJson(data.getString(CONFIRMED_APPOINTMENTIDS), new TypeToken<List<Integer>>() {
            }.getType());
            dbHelper.confirmAppointment(appointmentIDs, u);
            response.put(TYPE, SUCCESS);
            response.put(MESSAGE, ServerMessages.REFRESH_APPOINTMENT_INVITATIONS.getMessage());
        }else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject deleteParticipantRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            Group g = dbHelper.getGroup(data.getInt(GROUP_ID));
            if (g != null) {
                dbHelper.removeParticipantFromAppointment(g, data.getInt(APPOINTMENT_ID), u);
                response.put(TYPE, SUCCESS);
                response.put(GROUP, new Gson().toJson(dbHelper.getGroup(g.getId())));
                response.put(MESSAGE, ServerMessages.DELETE_APPOINTMENT_PARTICIPANTS.getMessage());
            } else {
                response.put(TYPE, FAIL);
                response.put(MESSAGE, ServerMessages.GROUP_DOESNT_EXCIST.getMessage());
            }
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    private synchronized JSONObject deleteAppointmentRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = new JSONObject();
        User u = dbHelper.isLogedIn(new Gson().fromJson(data.getString(TOKEN), UUID.class));
        if (u != null) {
            Group g = dbHelper.getGroup(data.getInt(GROUP_ID));
            if (g != null) {
                dbHelper.deleteAppointment(g, data.getInt(APPOINTMENT_ID));
                response.put(TYPE, SUCCESS);
                response.put(GROUP, new Gson().toJson(dbHelper.getGroup(g.getId())));
                response.put(MESSAGE, ServerMessages.DELETE_APPOINTMENT.getMessage());
            } else {
                response.put(TYPE, FAIL);
                response.put(MESSAGE, ServerMessages.GROUP_DOESNT_EXCIST.getMessage());
            }
        } else {
            response.put(TYPE, FAIL);
            response.put(MESSAGE, ServerMessages.MUST_BE_LOGED_IN.getMessage());
        }
        return response;

    }

    public int getID() {
        return ID;
    }

    private JSONObject doRequest(JSONObject data) throws JSONException, IOException {
        JSONObject response = null;
        switch (data.getString(TYPE)) {
            case REGISTER_NEW_USER:
                response = registerUserResponse(registerUserRequest(data));
                break;
            case LOGIN_USER:
                response = loginUserRequest(data);
                break;
            case GET_RATINGS:
                response = getRatingsRequest(data);
                break;
            case CREATE_NEWRATING:
                response = newRatingRequest(data);
                break;
            case GET_GROUPS_CONTAINING_USER:
                response = getGroupsRequest(data);
                break;
            case CREATE_NEW_GROUP:
                response = createNewGroupsRequest(data);
                break;
            case DELETE_GROUP:
                response = deleteGroupRequest(data);
                break;
            case INVITE_FRIEND:
                response = changeMembersInGroupRequest(data);
                break;
            case DELETE_FRIEND:
                response = changeMembersInGroupRequest(data);
                break;
            case CHECK_INVITATIONS:
                response = checkInvitations(data);
                break;
            case CONFIRM_INVITATIONS:
                response = confirmInvitations(data);
                break;
            case REGISTER_APPOINTMENT_FOR_GROUP:
                response = registerAppointmentRequest(data);
                break;
            case CONFIRM_APPOINTMENTS:
                response = confirmAppointmentsRequest(data);
                break;
            case DELETE_PARTICIPANT:
                response = deleteParticipantRequest(data);
                break;
            case DELETE_APPOINTMENT:
                response = deleteAppointmentRequest(data);
                break;
            default:
                response = commandNotFound(data);
                break;

        }
        return response;
    }

    public void run() {
        System.out.println("Server Thread " + ID + " running.");
        try {
            System.out.println();
            System.out.println("Connectet...");
            String clientSentence = streamIn.readUTF();
            try {
                JSONObject data = new JSONObject(clientSentence);
                System.out.println("Received: " + clientSentence);

                JSONObject dataToClient = doRequest(data);
                streamOut.writeUTF(dataToClient.toString() + '\n');
                System.out.println("Sended: " + dataToClient.toString());
                dbHelper.print();

                server.remove(ID);
            } catch (JSONException e) {
                System.out.println("Error: " + e.getMessage().toString());
                server.remove(ID);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println(ID + " IO ERROR reading: " + ioe.getMessage());
            server.remove(ID);
            interrupt();
        }

    }

    public void open() throws IOException, ClassNotFoundException {
        streamOut = new DataOutputStream(socket.getOutputStream());
        streamIn = new DataInputStream(socket.getInputStream());
    }


    public void close() throws IOException {
        if (socket != null) socket.close();
        if (streamIn != null) streamIn.close();
        if (streamOut != null) streamOut.close();
    }
}