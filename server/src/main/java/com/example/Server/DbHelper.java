package com.example.Server;

import com.example.DataObjects.Appointmant;
import com.example.DataObjects.Group;
import com.example.DataObjects.JunkMarket;
import com.example.DataObjects.LoginSys;
import com.example.DataObjects.Rating;
import com.example.DataObjects.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * Created by martin on 12.02.16.
 */
public class DbHelper implements Serializable {
    public int groupID = 0;
    public int appointmentID = 0;
    private ArrayList<User> users;
    private ArrayList<JunkMarket> junkMarkets;
    private ArrayList<Rating> ratings;
    private ArrayList<LoginSys> loginSys;
    private ArrayList<Group> groups;

    private static final long serialVersionUID = 1L;


    public DbHelper() {
        junkMarkets = new ArrayList<JunkMarket>();
        initJunkMarkets();
        users = new ArrayList<User>();
        loginSys = new ArrayList<LoginSys>();
        ratings = new ArrayList<Rating>();
        groups = new ArrayList<Group>();

    }

    private void initJunkMarkets() {
        try {
            JSONObject file = new JSONObject(readFile("troedlemaerkteWithDublicate_formated.json"));
            junkMarkets = new Gson().fromJson(file.getString("index"), new TypeToken<List<JunkMarket>>() {
            }.getType());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage().toString());
        } catch (IOException e) {
            System.out.println(e.getMessage().toString());
        } catch (JSONException e) {
            System.out.println(e.getMessage().toString());

        }
    }

    private String readFile(String file) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String everything = "";

        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            everything = sb.toString();
        } finally {
            br.close();
        }
        return everything;
    }

    public ArrayList<Rating> getRatings(int marketID) {
        ArrayList<Rating> ratingsfromMarket = new ArrayList<Rating>();
        for (Rating r : ratings) {
            if (r.getMarketID() == marketID) {
                ratingsfromMarket.add(r);
            }
        }
        return ratingsfromMarket;
    }

    public boolean addRating(Rating newRating) {
        for (Rating r : ratings) {
            if (r.getCreatorUsername().equals(newRating.getCreatorUsername()) && r.getMarketID() == newRating.getMarketID())
                return false;
        }
        ratings.add(newRating);
        return true;
    }

    public boolean addUser(User u) {
        if (getUser(u) == null) {
            users.add(u);
            u.printUser();
            return true;
        } else
            return false;
    }

    private LoginSys getLoginSys(String username) {
        for (LoginSys ls : loginSys) {
            if (ls.getUserName().equals(username))
                return ls;
        }
        return null;
    }

    public UUID checkUserlogin(String userName, String password) {
        for (User u : users) {
            if (u.getUsername().equals(userName) && u.getPassword().equals(password)) {
                LoginSys oldLoginentry = getLoginSys(userName);
                LoginSys newLoginentry = new LoginSys(u.getUsername());
                if (oldLoginentry != null) {
                    loginSys.set(loginSys.indexOf(oldLoginentry), newLoginentry);
                } else {
                    this.loginSys.add(newLoginentry);
                }
                return newLoginentry.getToken();
            }
        }
        return null;
    }

    public User isLogedIn(UUID token) {
        if (token != null) {
            for (LoginSys ls : loginSys) {
                if (token.equals(ls.getToken())) {
                    if (ls.checklogin(token) == true) {
                        for (User u : users) {
                            if (u.getUsername().equals(ls.getUserName()))
                                return u;
                        }
                    }

                }
            }
        }
        return null;
    }

    public boolean deleteGroup(Group group, String userWhoWantDelete) {
        if (group.getAddmin().equals(userWhoWantDelete) == true) {
            groups.remove(group);
            return true;
        } else {
            return false;
        }

    }

    public Group getGroup(int groupID) {
        for (Group group : groups) {
            if (group.getId() == groupID)
                return group;
        }
        return null;
    }

    public User getUser(User someUser) {
        for (User user : users) {
            if (user.compareUser(someUser) == true)
                return user;
        }
        return null;
    }

    public ArrayList<Group> getConfirmedGroups(User u) {
        ArrayList<Group> userGroups = new ArrayList<Group>();
        for (Group g : groups) {
            if (g.getUsers().contains(u)) {
                userGroups.add(g);
            }
        }
        return userGroups;
    }

    public ArrayList<Group> getGroups(User u) {
        ArrayList<Group> userGroups = new ArrayList<Group>();
        for (Group g : groups) {
            if (g.getAllUsers().contains(u)) {
                userGroups.add(g);
            }
        }
        return userGroups;
    }

    public void addGroup(Group newGroup) {
        groupID++;
        newGroup.setId(groupID);
        groups.add(newGroup);

    }

    public ArrayList<Group> getUnconfrimedGroups(User u) {
        ArrayList<Group> userGroups = new ArrayList<Group>();
        for (Group g : groups) {
            if (g.getInvitedUsers().contains(u) == true) {
                userGroups.add(g);
            }
        }
        return userGroups;
    }

    public void confirmGroupInvitation(ArrayList<Integer> groupIds, User u) {
        for (Integer groupId : groupIds) {
            Group g = getGroup(groupId);
            if(g != null) {
                g.addUserToGroup(u);
                g.removeUserInvitation(u);
                groups.set(groups.indexOf(g), g);
            }
        }
        for (Group group : getUnconfrimedGroups(u)) {
            group.removeUserInvitation(u);
            groups.set(groups.indexOf(group), group);
        }
    }

    public void print() {
        System.out.println("Groups: " + groups.size());
        for (Group g : groups) {
            g.print();
            System.out.println();
        }
        System.out.println("\nlogins");
        for (LoginSys log : loginSys) {
            System.out.println(log.print());
        }
        System.out.println("Users");
        for (User u : users) {
            System.out.print(u.getUsername() + " ");
        }
        System.out.println();
        System.out.println("junkMarkets: " + junkMarkets.size());

    }

    public void changeGroup(Group changedGroup) {
        groups.set(groups.indexOf(changedGroup), changedGroup);
    }

    public ArrayList<Appointmant> getUnconfirmtAppointments(User u) {
        ArrayList<Appointmant> unconfirmtAppointments = new ArrayList<Appointmant>();
        for (Group g : getConfirmedGroups(u)) {
            for (Appointmant a : g.getAppointmants()) {
                if (a.getInvitedFriends().contains(u) == true)
                    unconfirmtAppointments.add(a);
            }
        }
        return unconfirmtAppointments;
    }

    public JunkMarket getJunkmarket(int marketID) {
        for (JunkMarket junkMarket : junkMarkets) {
            if (junkMarket.getId() == marketID)
                return junkMarket;
        }
        return null;
    }

    public void addAppointment(Group g, Calendar date, int junkMarketID, User u) {
        JunkMarket jm = getJunkmarket(junkMarketID);
        Appointmant appointmant = new Appointmant(date, jm, u.getUsername());
        appointmant.getParticipants().add(u);
        for (User user : g.getUsers()) {
            if (user.equals(u) == false)
                appointmant.addinvitedFriend(user);
        }
        appointmentID++;
        appointmant.setId(appointmentID);
        g.getAppointmants().add(appointmant);

    }

    public void confirmAppointment(ArrayList<Integer> appointmentsIds, User u) {

        for (Group g : getConfirmedGroups(u)) {
            for (Appointmant appointmant : g.getAppointmants()) {
                if (appointmentsIds.contains(appointmant.getId()) == true) {
                    appointmant.addParticipant(u);
                }
                appointmant.removeInvitedFriend(u);
                groups.set(groups.indexOf(g), g);
            }
        }

    }

    public void removeFriendFromAllAppointments(User u, Group g) {
        ArrayList<Appointmant> appointmants = g.getAppointmants();
        for (Appointmant appointmant : appointmants) {
            if (appointmant.getParticipants().contains(u) == true) {
                appointmant.removeInvitedFriend(u);
                appointmant.removeParticipant(u);
            }
        }

    }

    public void removeParticipantFromAppointment(Group g, int appointmentID, User u) {
        Appointmant appointmant = getAppointment(g, appointmentID);
        appointmant.getParticipants().remove(u);
        g.getAppointmants().set(g.getAppointmants().indexOf(appointmant), appointmant);
        groups.set(groups.indexOf(g), g);
    }

    private Appointmant getAppointment(Group g, int appointmentID) {
        for (Appointmant appointmant : g.getAppointmants()) {
            if (appointmant.getId() == appointmentID)
                return appointmant;
        }
        return null;
    }

    public void deleteAppointment(Group g, int appointmentID) {
        g.getAppointmants().remove(getAppointment(g, appointmentID));
        groups.set(groups.indexOf(g), g);

    }
}
