package com.example.Server;

import java.util.Scanner;

/**
 * Created by martin on 11.02.16.
 */
public class ServerMain {
    public static void main(String args[]) {

        Server server = new Server(9999);
        Thread t = new Thread(server);
        t.start();
        Scanner s = new Scanner(System.in);
        while (!s.next().equals("stop")) ;

        server.keepRunning = false;
        server.stopServer();
        t.interrupt();

    }
}


