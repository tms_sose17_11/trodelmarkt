package DataObjects;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;

/**
 * Created by martin on 17.05.17.
 */

public class Group implements Serializable {
    private String name;
    private int id;
    private ArrayList<User> users;
    private ArrayList<User> invitedUsers;
    private ArrayList<Appointmant> appointmants;
    private String addmin;

    private static final long serialVersionUID = 1L;

    public Group(String name, String addmin) {
        this.name = name;
        this.id = -1;
        this.users = new ArrayList<User>();
        this.invitedUsers = new ArrayList<User>();
        this.appointmants = new ArrayList<Appointmant>();
        this.addmin = addmin;
    }

    private boolean checkIfUserIsInGroup(User u, AbstractList<User> userList) {
        boolean same = false;
        for (User user : userList) {
            if (user.compareUser(u))
                same = true;
        }
        return same;
    }

    public boolean checkIfUserIsInGroup(User u) {
        return (checkIfUserIsInGroup(u, users));
    }

    public boolean addUserToGroup(User u) {
        if (checkIfUserIsInGroup(u, users) == false) {
            users.add(u);
            return true;
        }
        return false;
    }

    public boolean addUserInvitation(User u) {
        if (checkIfUserIsInGroup(u, invitedUsers) == false) {
            users.add(u);
            return true;
        }
        return false;
    }

    public boolean removeUserfromGroup(User u) {
        if (checkIfUserIsInGroup(u, users) == false) {
            users.remove(u);
            return true;
        }
        return false;
    }

    public boolean removeUserInvitation(User u) {
        if (checkIfUserIsInGroup(u, invitedUsers) == true) {
            invitedUsers.remove(u);
            return true;
        }
        return false;
    }

    public ArrayList<User> getAllUsers() {
        ArrayList<User> allUsers = new ArrayList<User>();
        allUsers.addAll(users);
        allUsers.addAll(invitedUsers);
        return allUsers;
    }

    public ArrayList<Appointmant> getAppointmants() {
        return appointmants;
    }

    public void setAppointmants(ArrayList<Appointmant> appointmants) {
        this.appointmants = appointmants;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddmin() {
        return addmin;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public ArrayList<User> getInvitedUsers() {
        return invitedUsers;
    }

    public void print() {
        System.out.println("Admin: " + addmin);
        System.out.println("ID: " + id);
        System.out.println("Name: " + name);
        System.out.print("Users: ");
        for (User u : users) {
            System.out.print(u.getUsername() + " ");

        }
        System.out.println();
        System.out.print("InvitedUsers: ");
        for (User u : invitedUsers) {
            System.out.print(u.getUsername() + " ");

        }

    }

}
