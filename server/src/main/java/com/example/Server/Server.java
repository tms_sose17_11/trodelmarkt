package com.example.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class Server implements Runnable {
    private ServerThread clients[] = new ServerThread[50];
    private ServerSocket server = null;
    private int clientCount = 0;
    volatile boolean keepRunning = true;
    private DbHelper dbHelper = new DbHelper();

    public Server(int port) {
        try {
            readObject();
            dbHelper.print();
            System.out.println("Binding to port " + port + ", please wait  ...");
            server = new ServerSocket(port);
            System.out.println("Server started: " + server);
        } catch (IOException ioe) {
            System.out.println("Can not bind to port " + port + ": " + ioe.getMessage());
        }
    }

    private void readObject() {
        try {

            File f = new File("dbhelper.ser");
            if (f.exists() && f.length() != 0) {
                FileInputStream fis = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fis);
                dbHelper = (DbHelper) ois.readObject();
                ois.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    private void wirteObject() {
        try {
            FileOutputStream fout = new FileOutputStream("dbhelper.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(dbHelper);
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        while (keepRunning == true) {
            try {
                System.out.println("Waiting for a client ...");
                addThread(server.accept());
            } catch (IOException ioe) {
                System.out.println("Server accept error: " + ioe);
            }
        }
    }

    public void stopServer() {
        try {
            wirteObject();
            server.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private int findClient(int ID) {
        for (int i = 0; i < clientCount; i++)
            if (clients[i].getID() == ID)
                return i;
        return -1;
    }


    public void remove(int ID) {
        int pos = findClient(ID);
        if (pos >= 0) {
            ServerThread toTerminate = clients[pos];
            System.out.println("Removing client thread " + ID + " at " + pos);
            if (pos < clientCount - 1)
                for (int i = pos + 1; i < clientCount; i++)
                    clients[i - 1] = clients[i];
            clientCount--;
            try {
                toTerminate.close();
            } catch (IOException ioe) {
                System.out.println("Error closing thread: " + ioe);
            }
            toTerminate.interrupt();
        }
    }

    private synchronized void addThread(Socket socket) {
        System.out.println("Clients: " + clientCount);

        if (clientCount < clients.length) {
            System.out.println("Client accepted: " + socket);
            clients[clientCount] = new ServerThread(this, socket, dbHelper);
            try {
                clients[clientCount].open();
                clients[clientCount].start();
                clientCount++;
            } catch (IOException ioe) {
                System.out.println("Error opening thread: " + ioe);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else
            System.out.println("Client refused: maximum " + clients.length + " reached.");
    }


}
