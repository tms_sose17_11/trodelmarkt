package com.example.martin.myapplication.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.app.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.martin.myapplication.PrefUtils;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;
import com.example.martin.myapplication.ServerCommunication.ServerResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity implements ServerResponse {

    private EditText etxtUserName;
    private EditText etxtInputPassword;
    private ProgressDialog pd;
    private Requests requests;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        requests = new Requests(this);
        etxtUserName = (EditText) findViewById(R.id.etxt_login_username);
        etxtInputPassword = (EditText) findViewById(R.id.etxt_login_password);
        pd = new ProgressDialog(LoginActivity.this, R.style.MyTheme);
        pd.setCancelable(false);


    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_login_link_to_register_screen:
                Intent i = new Intent(LoginActivity.this,
                        RegisterActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.btn_login:
                String userName = etxtUserName.getText().toString().trim();
                String password = etxtInputPassword.getText().toString().trim();
                if (!userName.isEmpty() && !password.isEmpty()) {
                    pd.show();
                    requests.loginUser(password, userName);
                } else {
                    Toast.makeText(getApplicationContext(),
                            R.string.missing_information_error_message, Toast.LENGTH_LONG)
                            .show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void publishResult(JSONObject data) {
        pd.dismiss();
        try {
            if (data.getString(Requests.TYPE).equals(Requests.SUCCESS)) {
                PrefUtils.saveToPrefs(this, PrefUtils.PREFS_LOGIN_TOKEN_KEY, new Gson().fromJson(data.get(Requests.TOKEN).toString(), UUID.class));
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                Toast.makeText(getApplicationContext(),
                        data.getString(Requests.MESSAGE), Toast.LENGTH_LONG)
                        .show();
                finish();
            } else if (data.getString(Requests.TYPE).equals(Requests.FAIL)) {
                Toast.makeText(getApplicationContext(),
                        data.getString(Requests.MESSAGE), Toast.LENGTH_LONG)
                        .show();
            }
        } catch (JSONException e) {
            Log.e(Requests.JSONEXCEPTION, e.getMessage());
        }
    }
}

