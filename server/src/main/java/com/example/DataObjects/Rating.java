package com.example.DataObjects;

import java.io.Serializable;

/**
 * Created by martin on 15.05.17.
 */

public class Rating implements Serializable {
    private int marketID;
    private String creatorUsername;
    private int rating;
    private String comment;

    public Rating(int marketID, String name, int rating, String comment) {
        this.marketID = marketID;
        this.creatorUsername = name;
        this.rating = rating;
        this.comment = comment;
    }

    public String getCreatorUsername() {
        return creatorUsername;
    }

    public void setCreatorUsername(String creatorUsername) {
        this.creatorUsername = creatorUsername;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getMarketID() {
        return marketID;
    }

    public void setMarketID(int marketID) {
        this.marketID = marketID;
    }
}
