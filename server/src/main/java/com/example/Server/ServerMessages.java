package com.example.Server;

/**
 * Created by martin on 29.05.17.
 */

public enum ServerMessages {
    SUCCESSFULL_REGESTRATION("Sie wurden Registriert"),
    USER_ALREADY_EXCISTING("Der Username existiert bereits!"),
    SUCCESSFULL_LOGIN("Anmeldung erfolgreich"),
    FAIL_LOGIN("Ihr Passwort und/oder Username stimmen nicht"),
    SUCCESSFULLY_SAVE_RATING("Bewertung gespeichert"),
    ALLREADY_RATED("Sie haben schon eine Bewertung abgegeben"),
    MUST_BE_LOGED_IN("Sie sind nicht angemeldet. Melden Sie sich erst an um diese Funktionen zu nutzen"),
    SUCCESSFULL_CREATED_GROUP("Gruppe wurde erstellt"),
    SUCCESSFULL_DELETE_GROUP("Gruppe wurde gelöscht"),
    NOT_ADMIN("Sie sind nicht Administrator! Gruppe kann nur vom Administrator gelöscht werden"),
    GROUP_DOESNT_EXCIST("Gruppe exsistiert nicht mehr!"),
    REFRESH_GROUP_INVITATION("Gruppen einladungen wurden aktualisiert."),
    NO_SERVICE_ERROR("Fehler! Anfrage wurde nicht gefunden"),
    SUCCESSFULL_APPOINTMENT_REGISTRATION("Verabredungen wurde registriet"),
    REFRESH_APPOINTMENT_INVITATIONS("Verabredungen wurden aktualisiert."),
    DELETE_APPOINTMENT_PARTICIPANTS("Teilnehmer wurde entfernt"),
    USER_DOESNT_EXCIST("User %s existiert nicht"),
    SUCCESSFULL_DELETE_uSER("User %s wurde gelöscht"),
    USER_ALREADY_INVITED_CONFIRMED("User %s wurde bereits eingeladen"),
    USER_ALREADY_INVITED_UNCONFIRMED("User %s wurde bereits eingeladen, hat seine  Einladung aber noch nicht besätigt"),
    USER_INVITED("User %s wurde eingeladen"),
    DELETE_APPOINTMENT("Verabredung wurde gelöscht"),
    REGISTER_APOINTMENT_ERROR_NOT_A_MAMBER("Verabredungen wurde nicht registriert! Sie müssen die Gruppen Einladung erst annehmen");
    private String message;

    ServerMessages(String message) {
        this.message = message;
    }

    public String getMessage(String param) {
        return String.format(message, param);
    }

    public String getMessage() {
        return message;
    }
}
