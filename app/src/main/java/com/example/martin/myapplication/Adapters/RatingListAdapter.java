package com.example.martin.myapplication.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.martin.myapplication.R;

import java.util.List;

import DataObjects.Rating;

/**
 * Created by martin on 15.05.17.
 */

public class RatingListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Rating> ratings;

    public RatingListAdapter(Context context, List<Rating> ratings) {
        this.ratings = ratings;
        this.mContext = context;

    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    @Override
    public int getCount() {
        if (ratings != null) {
            return ratings.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (ratings != null) {
            return ratings.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (ratings != null) {
            return ratings.get(position).getMarketID();
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.rating_item, parent, false);
        }

        final Rating rating = (Rating) getItem(position);
        TextView txtUsername = (TextView) convertView.findViewById(R.id.txt_user_content);
        txtUsername.setText(rating.getCreatorUsername());

        RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.item_ratingBar);
        ratingBar.setRating(rating.getRating());
        return convertView;
    }
}
