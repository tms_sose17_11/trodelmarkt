package DataObjects;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by martin on 26.04.17.
 */

public class User implements Serializable {

    private String firstname;
    private String surname;
    private String username;
    private String password;


    private static final long serialVersionUID = 1L;

    public User(String firstname, String surname, String username, String password) {
        this.firstname = firstname;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public User(String username) {
        this.firstname = null;
        this.surname = null;
        this.username = username;
        this.password = null;
    }

    public boolean compareUser(User u) {
        if (username.equals(u.getUsername()))
            return true;
        else
            return false;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", surname='" + surname + '\'' +
                ", firstname='" + firstname + '\'' +
                '}';
    }

    public void printUser() {
        System.out.println("firstname: " + firstname);
        System.out.println("surname: " + surname);
        System.out.println("username: " + username);
        System.out.println("password: " + password);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof User)) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(firstname, user.firstname) && Objects.equals(surname, user.surname) && Objects.equals(username, user.username) && Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, surname, username, password);
    }
}
