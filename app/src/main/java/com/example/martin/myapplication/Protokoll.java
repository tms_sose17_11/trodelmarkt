package com.example.martin.myapplication;

/**
 * Created by martin on 03.06.17.
 */

public interface Protokoll {
    //Types
    //Types
    String CHECK_INVITATIONS = "checkInvitations";
    String CONFIRM_INVITATIONS = "confirmGroupInvitation";
    String CONFIRM_APPOINTMENTS = "confirmAppointments";
    String UNCONFIRMED_INVITATION = "unconfirmedInvitation";
    String FAIL = "fail";
    String SUCCESS = "success";
    String INVITE_FRIEND = "inviteFriend";
    String GET_GROUPS_CONTAINING_USER = "getGroups";
    String CREATE_NEW_GROUP = "createNewGroup";
    String REGISTER_APPOINTMENT_FOR_GROUP = "registerAppointment";
    String SUCCEESS_REGISTERT_APPOINTMENT = "success_registert_appointment";
    String LOGIN_USER = "loginUser";
    String REGISTER_NEW_USER = "registerUser";
    String GET_RATINGS = "getRatings";
    String CREATE_NEWRATING = "newRating";
    String DELETE_APPOINTMENT = "deleteAppointment";
    String DELETE_GROUP = "deleteGroup";
    String DELETE_FRIEND = "deleteFriend";
    String DELETE_PARTICIPANT = "deleteParticipant";


    String RATINGS = "ratings";
    String GROUPS = "groups";

    //params
    String USERNAME = "username";
    String TOKEN = "token";
    String GROUP_ID = "groupID";
    String GROUP = "group";
    String GROUP_NAME = "groupName";
    String APPOINTMENTTS = "appointments";
    String APPOINTMENT_DATE = "appointmant";
    String CONFIRMED_GROUPIDS = "confirmtGroupIds";
    String MARKET_ID = "marketID";
    String CONFIRMED_APPOINTMENTIDS = "confirmtAppointmentIDs";
    String PASSWORD = "password";
    String NEWUSER = "newUser";
    String RATING_FOR_MARKET = "rating";
    String COMMENT_FOR_RATING = "comment";
    String APPOINTMENT_ID = "appointmentID";

    String TYPE = "type";
    String MESSAGE = "message";



}
