package com.example.martin.myapplication.ServerCommunication;

import android.content.Context;
import android.util.Log;

import com.example.martin.myapplication.PrefUtils;
import com.example.martin.myapplication.Protokoll;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import DataObjects.User;

/**
 * Created by martin on 02.06.17.
 */

public class Requests implements Protokoll {
    public static final String JSONEXCEPTION = "JSONEXCEPTION";



    private Context context;
    private ServerResponse serverResponse;
    public Requests(ServerResponse serverResponse) throws IllegalArgumentException {
        if(serverResponse instanceof Context) {
            this.context = (Context) serverResponse;
            this.serverResponse = serverResponse;
        }else
            throw new IllegalArgumentException("ServerResponse must be Instance Context");
    }

    public void checkInvatations() {
        try {
            JSONObject data = new JSONObject();

            data.put(TYPE, CHECK_INVITATIONS);
            data.put(TOKEN, new Gson().toJson(PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY), UUID.class));
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e) {
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }

    public void confirmGroupInvitations(ArrayList<Integer> toConfirmGroupIDs) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, CONFIRM_INVITATIONS);
            data.put(TOKEN, new Gson().toJson(PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY), UUID.class));
            data.put(CONFIRMED_GROUPIDS, new Gson().toJson(toConfirmGroupIDs));
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e) {
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void confirmAppointmentInvitations(ArrayList<Integer> toConfirmAppointmentIDs) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, CONFIRM_APPOINTMENTS);
            data.put(TOKEN, new Gson().toJson(PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY), UUID.class));
            data.put(CONFIRMED_APPOINTMENTIDS, new Gson().toJson(toConfirmAppointmentIDs));
            ServerConnection.getInstance().startRequest(data, serverResponse);
        }catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void inviteFriendToGroup(String invitedFriendUsername, int groupID) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, INVITE_FRIEND);
            data.put(TOKEN, PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY));
            data.put(USERNAME, invitedFriendUsername);
            data.put(GROUP_ID, groupID);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        }catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void registerAppointment(Calendar appointmentDate, int marketID, int groupID) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, REGISTER_APPOINTMENT_FOR_GROUP);
            data.put(TOKEN, new Gson().toJson(PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY), UUID.class));
            data.put(APPOINTMENT_DATE, new Gson().toJson(appointmentDate, Calendar.class));
            data.put(MARKET_ID, marketID);
            data.put(GROUP_ID, groupID);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        }catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void getGroupsContainingUser() {
        try {
            JSONObject data  = new JSONObject();
            data.put(TYPE, GET_GROUPS_CONTAINING_USER);
            data.put(TOKEN, new Gson().toJson(PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY), UUID.class));
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());

        }
    }
    public void createGroup(String groupName) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, CREATE_NEW_GROUP);
            data.put(TOKEN, new Gson().toJson(PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY), UUID.class));
            data.put(GROUP_NAME, groupName);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());

        }
    }
    public void loginUser(String password, String userName) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, LOGIN_USER);
            data.put(USERNAME, userName);
            data.put(PASSWORD, password);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void registerUser(User newUser) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, REGISTER_NEW_USER);
            data.put(NEWUSER, new Gson().toJson(newUser, User.class));
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void getRatingsForMarket(int marketID) {
        try {
            JSONObject data  = new JSONObject();
            data.put(TYPE, GET_RATINGS);
            data.put(MARKET_ID, marketID);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void createNewRating(int marketID,  int rating, String comment) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, CREATE_NEWRATING);
            data.put(TOKEN, new Gson().toJson(PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY), UUID.class));
            data.put(MARKET_ID, marketID);
            data.put(RATING_FOR_MARKET, rating);
            data.put(COMMENT_FOR_RATING, comment);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e){
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void deletAppointment(int appointmentID,  int groupID) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, DELETE_APPOINTMENT);
            data.put(TOKEN, PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY));
            data.put(APPOINTMENT_ID, appointmentID);
            data.put(GROUP_ID, groupID);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e) {
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void deleteGroup(int groupID) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE,  DELETE_GROUP);
            data.put(TOKEN, PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY));
            data.put(GROUP_ID, groupID);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e) {
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void deleteFriend(int groupID, String username) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, DELETE_FRIEND);
            data.put(TOKEN, PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY));
            data.put(USERNAME, username);
            data.put(GROUP_ID, groupID);
            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e) {
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public void deleteParticipant(int groupID, int appointmentID, String username) {
        try {
            JSONObject data = new JSONObject();
            data.put(TYPE, DELETE_PARTICIPANT);
            data.put(TOKEN, PrefUtils.getFromPrefs(context, PrefUtils.PREFS_LOGIN_TOKEN_KEY));
            data.put(APPOINTMENT_ID, appointmentID);
            data.put(USERNAME, username);
            data.put(GROUP_ID, groupID);

            ServerConnection.getInstance().startRequest(data, serverResponse);
        } catch (JSONException e) {
            Log.e(JSONEXCEPTION, e.getMessage());
        }
    }
    public JSONObject connectionError() {
        try {
            JSONObject error = new JSONObject();
            error.put(TYPE, FAIL);
            error.put(MESSAGE, context.getString(com.example.martin.myapplication.R.string.server_connection_error_message));
            return error;
        } catch (JSONException e) {
            Log.e(JSONEXCEPTION, e.getMessage());
            return null;
        }
    }

}
