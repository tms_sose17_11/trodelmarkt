package com.example.martin.myapplication.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.martin.myapplication.Adapters.RatingListAdapter;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;
import com.example.martin.myapplication.ServerCommunication.ServerResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import DataObjects.Rating;

public class RatingActivity extends AppCompatActivity implements ServerResponse {
    private ListView listView;
    private ProgressDialog pd;
    private RatingBar ratingBar;
    private RatingListAdapter ratingListAdapter;
    private ArrayList<Rating> ratings;
    private int marketID;
    private Requests requests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        pd = new ProgressDialog(this, R.style.MyTheme);
        pd.setCancelable(false);
        requests = new Requests(this);
        ratingBar = (RatingBar) findViewById(R.id.rb_overall);
        marketID = getIntent().getExtras().getInt(MainActivity.INTANT_TAG_SELECTED_MARKET_ID);
        listView = (ListView) findViewById(R.id.rating_list);
        ratings = new ArrayList<Rating>();
        getRatings();
        ratingListAdapter = new RatingListAdapter(this, ratings);
        listView.setAdapter(ratingListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> av, View v, int position, long id) {
                showCommentDialog(position);
            }
        });

    }

    private void showCommentDialog(int position) {
        Dialog dialog = new Dialog(RatingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.comment_dialog);
        TextView txtComment = (TextView) dialog.findViewById(R.id.txt_rating_comment);
        Rating rating = (Rating) ratingListAdapter.getItem(position);
        txtComment.setText(rating.getComment());
        dialog.show();
    }

    private void getRatings() {
        pd.show();
        requests.getRatingsForMarket(marketID);

    }

    private float getAverageRating() {
        float averageRating = 0;
        for (Rating r : ratings) {
            averageRating += r.getRating();
        }
        averageRating = averageRating / ratings.size();
        return averageRating;
    }

    @Override
    public void publishResult(JSONObject data) {
        pd.dismiss();
        try {
            if (data.getString(Requests.TYPE).equals(Requests.RATINGS)) {
                ratings = new Gson().fromJson(data.getString(Requests.RATINGS), new TypeToken<List<Rating>>() {
                }.getType());
                ratingBar.setRating(getAverageRating());
                ratingListAdapter.setRatings(ratings);
                ratingListAdapter.notifyDataSetInvalidated();
            } else if (data.getString(Requests.TYPE).equals(Requests.SUCCESS)) {
                getRatings();
                Toast.makeText(getApplicationContext(), data.getString(Requests.MESSAGE), Toast.LENGTH_LONG).show();
            } else if (data.getString(Requests.TYPE).equals(Requests.FAIL)) {
                Toast.makeText(getApplicationContext(), data.getString(Requests.MESSAGE), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            Log.e(Requests.JSONEXCEPTION, e.getMessage());
        }
    }

    public void onClick(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_rating_dialog);
        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.rb_newRating);
        final EditText etxtComment = (EditText) dialog.findViewById(R.id.etxt_new_rating_comment);
        Button btnSave_rating = (Button) dialog.findViewById(R.id.btn_save_rating);
        btnSave_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                requests.createNewRating(marketID, (int) ratingBar.getRating(), etxtComment.getText().toString());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
