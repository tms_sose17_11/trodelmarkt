package com.example.martin.myapplication.ServerCommunication;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by martin on 22.04.17.
 */

public class ServerConnection {

    private static final String SERVERNAME = "141.45.204.178";
    private static final int SERVERPORT = 9999;

    private static final String UNKNOWN_HOST_EXCEPTION = "UnknownHostException";
    private static final String IOEXCEPTION = "IOException";
    private static final String EXCEPTION = "Exception";

    private static ServerConnection ourInstance = new ServerConnection();
    public static ServerConnection getInstance() {
        return ourInstance;
    }
    private ServerConnection() {

    }

    public void startRequest(JSONObject data, ServerResponse resiver) {
        Client myClient = new Client(data, resiver);
        myClient.execute();
    }
    private class Client extends AsyncTask<Void, Void, String> {
        private Socket socket = null;
        private DataInputStream streamIn = null;
        private DataOutputStream streamOut = null;
        private ServerResponse response = null;
        private JSONObject data;

        public Client( JSONObject data, ServerResponse resiver) {
            this.response = resiver;
            this.data = data;

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                this.response.publishResult(new JSONObject(result.toString()));
            } catch (JSONException e){
                Log.e(Requests.JSONEXCEPTION, e.getMessage());
            }
        }
        private String getServerError(){
            Requests requests = new Requests(response);
            return requests.connectionError().toString();
        }
        public void close() throws IOException {
            if (socket != null) socket.close();
            if (streamIn != null) streamIn.close();
            if (streamOut != null) streamOut.close();
        }


        @Override
        protected String doInBackground(Void... params) {

            String response = "";
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(SERVERNAME, SERVERPORT), 6000);
                streamOut = new DataOutputStream(socket.getOutputStream());
                streamOut.writeUTF(data.toString());
                streamIn = new DataInputStream(socket.getInputStream());
                response = streamIn.readUTF();
            }catch (UnknownHostException uhe) {
                Log.e(UNKNOWN_HOST_EXCEPTION, uhe.getMessage());
                return getServerError();
            } catch (IOException ioe) {
                Log.e(IOEXCEPTION, ioe.getMessage());
                return getServerError();
            } catch (Exception e) {
                Log.e(EXCEPTION, e.getMessage());
                return getServerError();
            } finally {
                try {
                    close();
                } catch (IOException e) {
                    Log.e(IOEXCEPTION, e.getMessage());
                }
            }
            return response;
        }
    }
}
