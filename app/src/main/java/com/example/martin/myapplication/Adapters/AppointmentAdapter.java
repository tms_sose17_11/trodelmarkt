package com.example.martin.myapplication.Adapters;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.martin.myapplication.Activities.GroupDetailActivity;
import com.example.martin.myapplication.Activities.MainActivity;
import com.example.martin.myapplication.ListFragment;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DataObjects.Appointmant;
import DataObjects.Group;

/**
 * Created by martin on 26.05.17.
 */

public class AppointmentAdapter extends ListAdapter {

    public static final int SELECT_APPOINTMENTS_VIEW = 1;
    public static final int ADMINISTRATE_APPOINTMENTS_VIEW = 2;

    private Group currentSelectetGroup;
    private String currentLogedInUsername;
    private boolean[] checked;
    private int whichView;
    private OnAdapterInteractionListener onAdapterInteractionListener;
    public AppointmentAdapter(Context context, List<Appointmant> appointments, int whichView) throws IllegalArgumentException {
        super(context, appointments);
        if (whichView == 1 || whichView == 2) {
            this.currentSelectetGroup = null;
            this.currentLogedInUsername = null;
            this.whichView = whichView;
            this.checked = new boolean[list.size()];
            for (int i = 0; i < checked.length; i++) {
                checked[i] = false;
            }
            this.whichView = whichView;
        } else
            throw new IllegalArgumentException();

    }

    CompoundButton.OnCheckedChangeListener mListener = new CompoundButton.OnCheckedChangeListener() {

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            checked[(Integer) buttonView.getTag()] = isChecked;
        }
    };

    public void setOnAdapterInteractionListener(OnAdapterInteractionListener onAdapterInteractionListener) {
        this.onAdapterInteractionListener = onAdapterInteractionListener;
    }

    public interface OnAdapterInteractionListener{
        void setParticipantsForAppoinment(Appointmant appoinment);
    }
    public ArrayList<Integer> getCheckeAppointmentIDs() {
        ArrayList<Integer> checkedGroupIDs = new ArrayList<Integer>();
        for (int i = 0; i < checked.length; i++) {
            if (checked[i] == true) {
                checkedGroupIDs.add(((Appointmant) list.get(i)).getId());
            }
        }
        return checkedGroupIDs;
    }

    public void setCurrentSelectetGroup(Group currentSelectetGroup) {
        this.currentSelectetGroup = currentSelectetGroup;
        list = currentSelectetGroup.getAppointmants();
    }

    public void setCurrentLogedInUsername(String currentLogedInUsername) {
        this.currentLogedInUsername = currentLogedInUsername;
    }

    private void setViewForSelectAppointments(View convertView, int position, final Appointmant appointmant) {
        initAppointmentDateTxt(convertView, appointmant.getDate().getTime());
        CheckBox cBox = (CheckBox) convertView.findViewById(R.id.cbox_appointment_accept);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.appointment_linearLayout_1);
        Button btnDeleteAppointment = (Button) convertView.findViewById(R.id.btn_delete_appointment);
        Button btnShowOnCard = (Button) convertView.findViewById(R.id.btn_show_on_card);
        if (btnDeleteAppointment != null) {
            linearLayout.removeView(btnDeleteAppointment);
        }
        cBox.setTag(Integer.valueOf(position));
        cBox.setChecked(false);
        cBox.setOnCheckedChangeListener(mListener);
        if (context instanceof MainActivity) {
            btnShowOnCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context instanceof MainActivity) {
                        ((MainActivity) context).setSelectedMarketID(appointmant.getMarket().getId());
                        ((MainActivity) context).showSelectetMarket();
                    }
                }
            });
        }
    }

    private void setViewForAdministrateAppoinments(View convertView, final Appointmant appointmant) {

        initAppointmentDateTxtWithListener(convertView, appointmant);
        CheckBox cBox = (CheckBox) convertView.findViewById(R.id.cbox_appointment_accept);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.appointment_linearLayout_1);
        if (cBox != null)
            linearLayout.removeView(cBox);
        if (currentSelectetGroup != null && currentLogedInUsername != null) {
            setVisibilityDeleteAppointmentBtn(convertView, appointmant);
        }
        initShowOnCardBtn(convertView, appointmant.getMarket().getId());

    }

    private void setOnclicklistenerForDeleteBtn(Button btnDeleteAppointment, final int appointmentID, final int groupID) {
        btnDeleteAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof GroupDetailActivity) {
                    ((GroupDetailActivity) context).showProgressDialog();
                    Requests requests = new Requests((GroupDetailActivity) context);
                    requests.deletAppointment(appointmentID, groupID);
                }
            }
        });
    }

    private void setVisibilityDeleteAppointmentBtn(View convertView, Appointmant appointmant) {
        Button btnDeleteAppointment = (Button) convertView.findViewById(R.id.btn_delete_appointment);
        if (currentSelectetGroup.getAddmin().equals(currentLogedInUsername) == false) {
            if (appointmant.getCreator().equals(currentLogedInUsername)) {
                btnDeleteAppointment.setVisibility(View.VISIBLE);
                setOnclicklistenerForDeleteBtn(btnDeleteAppointment, appointmant.getId(), currentSelectetGroup.getId());
            } else {
                btnDeleteAppointment.setVisibility(View.INVISIBLE);
            }
        } else {
            btnDeleteAppointment.setVisibility(View.VISIBLE);
            setOnclicklistenerForDeleteBtn(btnDeleteAppointment, appointmant.getId(), currentSelectetGroup.getId());
        }
    }

    private void initShowOnCardBtn(View convertView, final int marketID) {
        Button btnShowOnCard = (Button) convertView.findViewById(R.id.btn_show_on_card);
        btnShowOnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, MainActivity.class);
                i.putExtra(MainActivity.INTANT_TAG_SELECTED_MARKET_ID, marketID);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(i);
            }
        });
    }

    private void initAppointmentDateTxt(View convertView, Date date) {
        TextView txtAppointmentDate = (TextView) convertView.findViewById(R.id.txt_appointment_date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(context.getString(R.string.date_format_appointment));
        txtAppointmentDate.setText(simpleDateFormat.format(date));
    }

    private void initAppointmentDateTxtWithListener(View convertView, final Appointmant appointmant) {
        TextView txtAppointmentDate = (TextView) convertView.findViewById(R.id.txt_appointment_date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(context.getString(R.string.date_format_appointment));
        txtAppointmentDate.setText(simpleDateFormat.format(appointmant.getDate().getTime()));
        txtAppointmentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAdapterInteractionListener.setParticipantsForAppoinment(appointmant);
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.appointment_item, parent, false);
        }
        final Appointmant appointmant = (Appointmant) getItem(position);

        switch (whichView) {
            case SELECT_APPOINTMENTS_VIEW:
                setViewForSelectAppointments(convertView, position, appointmant);
                break;
            case ADMINISTRATE_APPOINTMENTS_VIEW:
                setViewForAdministrateAppoinments(convertView, appointmant);
                break;
            default:
                break;
        }
        return convertView;
    }
}
