package com.example.martin.myapplication.Adapters;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.martin.myapplication.Activities.GroupDetailActivity;
import com.example.martin.myapplication.Activities.GroupsActivity;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;

import java.util.ArrayList;
import java.util.List;

import DataObjects.Group;

/**
 * Created by martin on 17.05.17.
 */

public class GroupListAdapter extends ListAdapter {

    public static final int SELECT_GROUP_FOR_REGISTER_APPOINTMENT_VIEW = 1;
    public static final int SELECT_GROUPS_FOR_CONFRIM_INVITATION_VIEW = 2;
    public static final int ADMINISTRATE_GROUPS_VIEW = 3;

    private String currentLogedInUsername;
    private boolean[] checked;
    private int whichView;

    public GroupListAdapter(Context context, List<Group> groups, int whichView) {
        super(context, groups);
        if (whichView == 1 || whichView == 2 || whichView == 3) {

            this.currentLogedInUsername = "";
            this.checked = new boolean[groups.size()];
            for (int i = 0; i < checked.length; i++) {
                checked[i] = false;
            }
            this.whichView = whichView;
        } else
            throw new IllegalArgumentException();

    }

    public void setCurrentLogedInUsername(String currentLogedInUsername) {
        this.currentLogedInUsername = currentLogedInUsername;
    }

    public void setGroups(List<Group> groups) {
        list = groups;
    }


    @Override
    public long getItemId(int position) {
        if (list != null) {
            return ((Group) list.get(position)).getId();
        }
        return 0;
    }

    CompoundButton.OnCheckedChangeListener checkboxListener = new CompoundButton.OnCheckedChangeListener() {

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            checked[(Integer) buttonView.getTag()] = isChecked;
        }
    };

    public ArrayList<Integer> getCheckeGroupsIDs() {
        ArrayList<Integer> checkedGroupIDs = new ArrayList<Integer>();
        for (int i = 0; i < checked.length; i++) {
            if (checked[i] == true) {
                checkedGroupIDs.add(((Group) list.get(i)).getId());
            }
        }
        return checkedGroupIDs;
    }

    private void setSelectGroupsForConfrimInvitationView(View view, final int position, final Group group) {
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.group_item_linearLayout_1);
        TextView txtGroupName = (TextView) view.findViewById(R.id.txt_group_name);
        LinearLayout.LayoutParams txtGroupNameLayoutParams = (LinearLayout.LayoutParams) txtGroupName.getLayoutParams();
        txtGroupName.setText(group.getName());
        CheckBox cBox = (CheckBox) view.findViewById(R.id.cbox_group_accept);
        Button btnDetail = (Button) view.findViewById(R.id.btn_detail_group);
        Button btnDeleteGroup = (Button) view.findViewById(R.id.btn_delete_group);
        if (btnDeleteGroup != null && btnDetail != null) {
            linearLayout.removeView(btnDeleteGroup);
            linearLayout.removeView(btnDetail);
        }
        cBox.setTag(Integer.valueOf(position));
        cBox.setChecked(false);
        cBox.setOnCheckedChangeListener(checkboxListener);
        txtGroupNameLayoutParams.weight = 0.6f;
        txtGroupName.setLayoutParams(txtGroupNameLayoutParams);
    }

    private void setSelectGroupForRegisterAppointmentView(View view, final Group group) {
        Button btnDeleteGroup = (Button) view.findViewById(R.id.btn_delete_group);
        Button btnDetail = (Button) view.findViewById(R.id.btn_detail_group);
        CheckBox cBox = (CheckBox) view.findViewById(R.id.cbox_group_accept);
        TextView txtGroupName = (TextView) view.findViewById(R.id.txt_group_name);
        txtGroupName.setText(group.getName());
        LinearLayout linearLayout1 = (LinearLayout) view.findViewById(R.id.group_item_linearLayout_1);
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.group_item_linearLayout_2);
        LinearLayout.LayoutParams layoutParamsTxtGroupName = (LinearLayout.LayoutParams) txtGroupName.getLayoutParams();
        LinearLayout.LayoutParams linearLayout2LayoutParams = (LinearLayout.LayoutParams) linearLayout2.getLayoutParams();
        if (cBox != null)
            linearLayout1.removeView(cBox);
        if (btnDeleteGroup != null && btnDetail != null) {
            linearLayout1.removeView(btnDeleteGroup);
            linearLayout1.removeView(btnDetail);
        }
        layoutParamsTxtGroupName.weight = 0.7f;
        txtGroupName.setLayoutParams(layoutParamsTxtGroupName);
        linearLayout2LayoutParams.weight = 0.3f;
        linearLayout2.setLayoutParams(linearLayout2LayoutParams);
    }

    private void setAdministrateGroupsView(View view, final Group group) {
        Button btnDeleteGroup = (Button) view.findViewById(R.id.btn_delete_group);
        Button btnDetail = (Button) view.findViewById(R.id.btn_detail_group);
        CheckBox cBox = (CheckBox) view.findViewById(R.id.cbox_group_accept);
        TextView txtGroupName = (TextView) view.findViewById(R.id.txt_group_name);
        txtGroupName.setText(group.getName());

        LinearLayout linearLayout1 = (LinearLayout) view.findViewById(R.id.group_item_linearLayout_1);
        if (cBox != null)
            linearLayout1.removeView(cBox);
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof GroupsActivity) {
                    Intent i = new Intent(context, GroupDetailActivity.class);
                    i.putExtra(GroupsActivity.INTANT_TAG_GROUP, group);
                    i.putExtra(GroupsActivity.INTANT_TAG_USERNAME, currentLogedInUsername);
                    context.startActivity(i);
                }

            }
        });
        if (group.getAddmin().equals(currentLogedInUsername) == false) {
            btnDeleteGroup.setVisibility(View.INVISIBLE);
        } else {
            btnDeleteGroup.setVisibility(View.VISIBLE);
            btnDeleteGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context instanceof GroupsActivity) {
                        ((GroupsActivity) context).progressDialogShow();
                        Requests requests = new Requests(((GroupsActivity) context));
                        requests.deleteGroup(group.getId());
                    }
                }
            });
        }
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.group_item, parent, false);
        }

        final Group group = (Group) getItem(position);

        TextView amountPersons = (TextView) view.findViewById(R.id.txt_content_amount_people);
        amountPersons.setText(Integer.toString(group.getUsers().size()));
        switch (whichView) {
            case SELECT_GROUP_FOR_REGISTER_APPOINTMENT_VIEW:
                setSelectGroupForRegisterAppointmentView(view, group);
                break;
            case SELECT_GROUPS_FOR_CONFRIM_INVITATION_VIEW:
                setSelectGroupsForConfrimInvitationView(view, position, group);
                break;
            case ADMINISTRATE_GROUPS_VIEW:
                setAdministrateGroupsView(view, group);
                break;
            default:
                break;
        }
        return view;
    }


}