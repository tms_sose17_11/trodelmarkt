package com.example.martin.myapplication.Activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.martin.myapplication.Adapters.AppointmentAdapter;
import com.example.martin.myapplication.Adapters.FriendsAdapter;
import com.example.martin.myapplication.ListFragment;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;
import com.example.martin.myapplication.ServerCommunication.ServerResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import DataObjects.Appointmant;
import DataObjects.Group;

public class GroupDetailActivity extends AppCompatActivity implements ServerResponse, AppointmentAdapter.OnAdapterInteractionListener {

    private Group group;
    private String userName;
    private FriendsAdapter friendsAdapter;
    private AppointmentAdapter appointmentAdapter;
    private ListFragment friendsListFragment;
    private ListFragment appointmentsListFragment;
    private ProgressDialog pd;
    private TextView txtInvitedFriendUsername;
    private Requests requests;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        pd = new ProgressDialog(GroupDetailActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
        group = (Group) getIntent().getSerializableExtra(GroupsActivity.INTANT_TAG_GROUP);
        userName = getIntent().getStringExtra(GroupsActivity.INTANT_TAG_USERNAME);
        txtInvitedFriendUsername = (TextView) findViewById(R.id.etxt_invite_username);
        textView = (TextView) findViewById(R.id.txt_which_user_is_seen);
        textView.setText(R.string.group_members);
        initAdapters();
        requests = new Requests(this);
        friendsListFragment = ListFragment.newInstance();
        appointmentsListFragment = ListFragment.newInstance();
        setFriendsListFragment();
        setExistenceinviteFriendBtn();
    }

    private void initAdapters() {
        friendsAdapter = new FriendsAdapter(this, group, userName);
        appointmentAdapter = new AppointmentAdapter(this, group.getAppointmants(), AppointmentAdapter.ADMINISTRATE_APPOINTMENTS_VIEW);
        appointmentAdapter.setCurrentLogedInUsername(userName);
        appointmentAdapter.setCurrentSelectetGroup(group);
        appointmentAdapter.setOnAdapterInteractionListener(this);
    }

    private void setExistenceinviteFriendBtn() {
        LinearLayout LinearLayout1GroupDetail = (LinearLayout) findViewById(R.id.group_detail_linearLayout_1);
        Button btnInviteFriend = (Button) findViewById(R.id.btn_add_friend);
        if (txtInvitedFriendUsername != null && btnInviteFriend != null) {
            if (group.getAddmin().equals(userName) == false) {
                LinearLayout1GroupDetail.removeView(txtInvitedFriendUsername);
                LinearLayout1GroupDetail.removeView(btnInviteFriend);

            }
        }
    }

    private void setFriendsListFragment() {
        FragmentManager fm = getFragmentManager();
        friendsAdapter.setGroupWithAllUsers(group);
        friendsListFragment.setListAdapter(friendsAdapter);
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, friendsListFragment).commit();
        textView.setText(R.string.group_members);

    }

    private void setAppointmentsListFragment() {
        FragmentManager fm = getFragmentManager();
        appointmentsListFragment.setListAdapter(appointmentAdapter);
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, appointmentsListFragment).commit();
        textView.setText(R.string.appointments);

    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_add_friend:
                pd.show();
                requests.inviteFriendToGroup(txtInvitedFriendUsername.getText().toString(), group.getId());
                break;
            case R.id.btn_see_friends:
                setFriendsListFragment();
                break;
            case R.id.btn_see_appointments:
                setAppointmentsListFragment();
                break;
            default:
                break;
        }
    }
    public void setParticipantsForAppoinment(Appointmant appointmant){
        FragmentManager fm = getFragmentManager();
        textView.setText(R.string.appointment_participant);
        friendsAdapter.setShowOnlyParticipants(appointmant);
        friendsListFragment.setListAdapter(friendsAdapter);
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, friendsListFragment).commit();
    }


    public void showProgressDialog() {
        pd.dismiss();
    }

    @Override
    public void publishResult(JSONObject data) {
        pd.dismiss();
        try {
            if (data.getString(Requests.TYPE).equals(Requests.SUCCESS)) {
                group = new Gson().fromJson(data.getString(Requests.GROUP), Group.class);
                appointmentAdapter.setCurrentSelectetGroup(group);
                setFriendsListFragment();
                Toast.makeText(getApplicationContext(),
                        data.getString(Requests.MESSAGE), Toast.LENGTH_LONG)
                        .show();
            } else if (data.getString(Requests.TYPE).equals(Requests.FAIL)) {
                Toast.makeText(getApplicationContext(),
                        data.getString(Requests.MESSAGE), Toast.LENGTH_LONG)
                        .show();
            }
        } catch (JSONException e) {
            Log.e(Requests.JSONEXCEPTION, e.getMessage());

        }

    }

}
