package DataObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by martin on 26.05.17.
 */

public class Appointmant implements Serializable {
    private Calendar date;
    private ArrayList<User> participants;
    private ArrayList<User> invitedFriends;
    private JunkMarket market;
    private int id;
    private String creator;


    public Appointmant(Calendar date, JunkMarket market, String creator) {
        this.date = date;
        this.participants = new ArrayList<User>();
        this.invitedFriends = new ArrayList<User>();

        this.market = market;
        this.creator = creator;
    }

    public ArrayList<User> getInvitedFriends() {
        return invitedFriends;
    }

    public void setInvitedFriends(ArrayList<User> invitedFriends) {
        this.invitedFriends = invitedFriends;
    }

    public void addParticipant(User user) {
        if (participants.contains(user) == false)
            participants.add(user);
    }

    public void addinvitedFriend(User user) {
        if (invitedFriends.contains(user) == false)
            invitedFriends.add(user);
    }

    public void removeInvitedFriend(User user) {
        if (invitedFriends.contains(user) == true)
            invitedFriends.remove(user);
    }

    public void removeParticipant(User user) {
        if (participants.contains(user) == true)
            participants.remove(user);
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public ArrayList<User> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<User> participants) {
        this.participants = participants;
    }

    public JunkMarket getMarket() {
        return market;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setMarket(JunkMarket market) {
        this.market = market;
    }

    public void print() {
        System.out.println("Date: " + date.getTime().toString());
        System.out.println("Market: " + market.getId() + " " + market.getBezirk());
        System.out.print("Teilnehmer: ");
        for (User u : participants) {
            System.out.print(u.getUsername() + " ");
        }
        System.out.println();
    }
}