package com.example.martin.myapplication;

import android.content.Context;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;

import java.util.UUID;


/**
 * Created by martin on 27.04.17.
 */

public class PrefUtils {
    public static final String PREFS_LOGIN_TOKEN_KEY = "__LOGINTOKEN__";
    private static final String EXCEPTION = "Exception";

    public static void saveToPrefs(Context context, String key, UUID value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(value);
        editor.putString(key, json);
        editor.commit();
    }

    public static void deletefromPrefs(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.commit();
    }

    public static UUID getFromPrefs(Context context, String key) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            Gson gson = new Gson();
            String json = sharedPrefs.getString(key, "notLogin");
            UUID token = null;
            if (json.equals("notLogin")) {
                token = UUID.fromString(json);
            } else
                token = gson.fromJson(json, UUID.class);
            return token;
        } catch (Exception e) {
            Log.e(EXCEPTION, e.getMessage());
            return null;
        }
    }
}
