package com.example.martin.myapplication.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.martin.myapplication.Activities.GroupDetailActivity;
import com.example.martin.myapplication.R;
import com.example.martin.myapplication.ServerCommunication.Requests;

import DataObjects.Appointmant;
import DataObjects.Group;
import DataObjects.User;

/**
 * Created by martin on 18.05.17.
 */

public class FriendsAdapter extends ListAdapter {


    private Group currentSelectetGroup;
    private String currentLogedInUserName;
    private Appointmant appointmant;

    public FriendsAdapter(Context context, Group group, String userName) {
        super(context, group.getAllUsers());
        this.currentSelectetGroup = group;
        this.currentLogedInUserName = userName;
        appointmant = null;

    }

    @Override
    public long getItemId(int position) {
        if (list != null) {
            return position;
        }
        return 0;
    }

    public void setGroupWithAllUsers(Group g) {
        this.currentSelectetGroup = g;
        appointmant = null;
        list = currentSelectetGroup.getAllUsers();
        notifyDataSetChanged();
    }

    public void setShowOnlyParticipants(Appointmant appointmant) {
        this.appointmant = appointmant;
        list = appointmant.getParticipants();
    }

    private void setColorImageDot(View view, User user) {
        final ImageView dot = (ImageView) view.findViewById(R.id.imv_invited_dot);
        if (currentSelectetGroup.getUsers().contains(user) == true) {
            Drawable green_dot = context.getDrawable(R.drawable.circled_dot_green);
            dot.setImageDrawable(green_dot);
        } else {
            Drawable green_red = context.getDrawable(R.drawable.circled_dot_red);
            dot.setImageDrawable(green_red);
        }
    }

    private void setVisibilityDeleteFriendBtn(View view, final User user) {
        Button deleteFriendBtn = (Button) view.findViewById(R.id.btn_delete_friend);
        TextView txtUsername = (TextView) view.findViewById(R.id.txt_invited_friend_username);
        txtUsername.setText(user.getUsername());
        if (currentSelectetGroup.getAddmin().equals(currentLogedInUserName) == true) {
            if (txtUsername.getText().equals(currentLogedInUserName)) {
                deleteFriendBtn.setVisibility(View.INVISIBLE);
            } else {
                deleteFriendBtn.setVisibility(View.VISIBLE);
                setOnClickListenerDeleteBtn(deleteFriendBtn, user);
            }
        } else {
            if (!txtUsername.getText().equals(currentLogedInUserName)) {
                deleteFriendBtn.setVisibility(View.INVISIBLE);
            } else {
                deleteFriendBtn.setVisibility(View.VISIBLE);
                setOnClickListenerDeleteBtn(deleteFriendBtn, user);
            }
        }
    }

    private void setOnClickListenerDeleteBtn(Button deleteFriendBtn, final User user) {
        deleteFriendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof GroupDetailActivity) {
                    ((GroupDetailActivity) context).showProgressDialog();
                    Requests requests = new Requests((GroupDetailActivity) context);
                    if (appointmant != null) {
                        requests.deleteParticipant(currentSelectetGroup.getId(), appointmant.getId(), user.getUsername());
                    } else {
                        requests.deleteFriend(currentSelectetGroup.getId(), user.getUsername());
                    }
                    if (user.getUsername().equals(currentLogedInUserName) && appointmant == null) {
                        ((GroupDetailActivity) context).finish();
                    }
                }
            }
        });

    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.friends_item, parent, false);
        }

        final User user = (User) getItem(position);
        setColorImageDot(view, user);
        setVisibilityDeleteFriendBtn(view, user);

        return view;
    }


}