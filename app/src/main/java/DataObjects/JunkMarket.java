package DataObjects;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by martin on 12.02.16.
 */
public class JunkMarket implements Serializable {
    private int id;
    private String bezirk;
    private String location;
    private String latitude;
    private String longitude;
    private String tage;
    private String zeiten;
    private String betreiber;
    private String email;
    private String www;
    private String bemerkungen;
    private ArrayList<Integer> multibleMarketsOnThisPlace;

    private static final long serialVersionUID = 1L;


    public JunkMarket() {
        multibleMarketsOnThisPlace = new ArrayList<Integer>();
    }


    public ArrayList<Integer> getMultibleMarketsOnThisPlace() {
        return multibleMarketsOnThisPlace;
    }

    public void setMultibleMarketsOnThisPlace(ArrayList<Integer> multibleMarketsOnThisPlace) {
        this.multibleMarketsOnThisPlace = multibleMarketsOnThisPlace;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBezirk() {
        return bezirk;
    }

    public void setBezirk(String bezirk) {
        this.bezirk = bezirk;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTage() {
        return tage;
    }

    public void setTage(String tage) {
        this.tage = tage;
    }

    public String getZeiten() {
        return zeiten;
    }

    public void setZeiten(String zeiten) {
        this.zeiten = zeiten;
    }

    public String getBetreiber() {
        return betreiber;
    }

    public void setBetreiber(String betreiber) {
        this.betreiber = betreiber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getBemerkungen() {
        return bemerkungen;
    }

    public void setBemerkungen(String bemerkungen) {
        this.bemerkungen = bemerkungen;
    }
}
